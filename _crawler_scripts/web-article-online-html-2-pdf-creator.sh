#!/bin/bash
# Use -gt 1 to consume two arguments per pass in the loop (e.g. each
# argument has a corresponding value to go with it).
# Use -gt 0 to consume one or more arguments per pass in the loop (e.g.
# some arguments don't have a corresponding value to go with it such
# as in the --default example).
# note: if this is set to -gt 0 the /etc/hosts part is not recognized ( may be a bug )

function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  "$1" |
   awk -F$fs '{
      indent = length("$1")/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         #printf("--%s-- prefix: --%s--\n", $2, "'$prefix'"); 
         if($2 == "'$prefix'"){
           printf("%s", $3);
         }else{
           #printf("000001%s2%s3%s=\"4%s5\"\n", "'$prefix'",vn, $2, $3);
         }
      }
   }'
}

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -s|--searchpath)
    SEARCHPATH="$2"
    shift # past argument
    ;;
    -v|--verbose)
    VERBOSE="$2"
    shift # past argument
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

echo SEARCH PATH     = "${SEARCHPATH}"

scriptexecutiondirectorytemp="$PWD"
find "${SEARCHPATH}" -type f -name '*.md' | while read fullfilename; do
    fulldirname="${fullfilename%/*}"
    filename=$(basename "$fullfilename")
    fname="${filename%.*}"
    cd "$fulldirname"
    if [ -f "$fname.pdf" ]
	then
		echo "Processing $fullfilename"
		mv "$fname.md" "$fname.yaml"
		sed -i -e "s/offline_file: \"\"/offline_file: $fname.pdf/g" "$fname.yaml"
	else
		content_id=$(parse_yaml "$filename" id)
		thumbnail_url=$(parse_yaml "$filename" thumbnail_urls)
		echo "Processing $fullfilename ----------> $content_id"
		if [ -n "$content_id" ]; then
		  if [ "$VERBOSE" == "yes" ]
		  then
			weasyprint "$content_id" "$fname.pdf"
		  else
			weasyprint "$content_id" "$fname.pdf" &> /dev/null
		  fi
		  if [ -f "$fname.pdf" ]
		  then
			mv "$fname.md" "$fname.yaml"
			sed -i -e "s/offline_file: \"\"/offline_file: $fname.pdf/g" "$fname.yaml"
			if [ -n "$thumbnail_url" ]; then
				wget "$thumbnail_url"
			fi
		  fi
		fi
	fi
    cd "$scriptexecutiondirectorytemp"
done

