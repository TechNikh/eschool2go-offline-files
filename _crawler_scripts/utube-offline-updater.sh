#!/bin/bash
# Use -gt 1 to consume two arguments per pass in the loop (e.g. each
# argument has a corresponding value to go with it).
# Use -gt 0 to consume one or more arguments per pass in the loop (e.g.
# some arguments don't have a corresponding value to go with it such
# as in the --default example).
# note: if this is set to -gt 0 the /etc/hosts part is not recognized ( may be a bug )

function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  "$1" |
   awk -F$fs '{
      indent = length("$1")/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         #printf("--%s-- prefix: --%s--\n", $2, "'$prefix'"); 
         if($2 == "'$prefix'"){
           printf("%s", $3);
         }else{
           #printf("000001%s2%s3%s=\"4%s5\"\n", "'$prefix'",vn, $2, $3);
         }
      }
   }'
}

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -f|--format)
    FORMAT="$2"
    shift # past argument
    ;;
    -s|--searchpath)
    SEARCHPATH="$2"
    shift # past argument
    ;;
    -md|--mdgitpath)
    MDGITPATH="$2"
    shift # past argument
    ;;
    -v|--verbose)
    VERBOSE="$2"
    shift # past argument
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done
echo MD GIT PATH  = "${MDGITPATH}"
echo SEARCH PATH     = "${SEARCHPATH}"

scriptexecutiondirectorytemp="$PWD"
find "${SEARCHPATH}" -type f -name '*.md' | while read fullfilename; do
    fulldirname="${fullfilename%/*}"
    filename=$(basename "$fullfilename")
    fname="${filename%.*}"
    cd "$fulldirname"
    if [ -f "$fname.webm" ] || [ -f "$fname.mp4" ] || [ -f "$fname.mkv" ]
	then
		echo "Processing $fullfilename"
		[ -f "$fname.webm" ] && videofilename="$fname.webm"
		[ -f "$fname.mp4" ] && videofilename="$fname.mp4"
		[ -f "$fname.mkv" ] && videofilename="$fname.mkv"

		# http://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
		encoded_download_url_path=$(python -c "import urllib; print urllib.quote('''$fulldirname/$videofilename''')")
		download_url_http_status=$(curl --write-out %{http_code} --silent --output /dev/null "https://s3.amazonaws.com/eschool2go-offline/$encoded_download_url_path")
		if [ "$download_url_http_status" == "200" ]
		then
			#echo "videofilename: $videofilename"
			#echo "fulldirname: $fulldirname"
			mv "$fname.md" "$fname.yaml"
			# http://askubuntu.com/questions/76808/how-do-i-use-variables-in-a-sed-command
			# s;pattern;replacement;
			# TODO: If there's ; in path or file name?
			# escape special characters like & in variable http://stackoverflow.com/questions/9483171/escaping-special-characters-in-bash-variables
			#escaped_download_url_path="$(echo "$fulldirname/$videofilename" | sed -e 's/[()&]/\\&/g')"
			escaped_videofilename="$(echo "$videofilename" | sed -e 's/[()&]/\\&/g')"
			escaped_fname="$(echo "$fname" | sed -e 's/[()&]/\\&/g')"
			#echo "fulldirname: $fulldirname ; download_url_path: $escaped_download_url_path"
			sed -i -e 's;download_url: "";download_url: "https://s3.amazonaws.com/eschool2go-offline/'"$encoded_download_url_path"'";g' "$fname.yaml"
			sed -i -e "s/offline_file: \"\"/offline_file: \"$escaped_videofilename\"/g" "$fname.yaml"
			sed -i -e "s/offline_thumbnail: \"\"/offline_thumbnail: \"$escaped_fname.jpg\"/g" "$fname.yaml"

			sed -i -e 's;download_url: "";download_url: "https://s3.amazonaws.com/eschool2go-offline/'"$encoded_download_url_path"'";g' "${MDGITPATH}/$fulldirname/$fname.md"
		else
			echo "failed. response: $download_url_http_status https://s3.amazonaws.com/eschool2go-offline/$encoded_download_url_path"
		fi
    fi
    cd "$scriptexecutiondirectorytemp"
done

