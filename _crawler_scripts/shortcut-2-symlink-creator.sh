#!/bin/bash
# Use -gt 1 to consume two arguments per pass in the loop (e.g. each
# argument has a corresponding value to go with it).
# Use -gt 0 to consume one or more arguments per pass in the loop (e.g.
# some arguments don't have a corresponding value to go with it such
# as in the --default example).
# note: if this is set to -gt 0 the /etc/hosts part is not recognized ( may be a bug )
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -o|--outputpath)
    OUTPUTPATH="$2"
    shift # past argument
    ;;
    -d|--outputdirectorytree)
    OUTPUTDIRECTORYTREE="$2"
    shift # past argument
    ;;
    -s|--searchpath)
    SEARCHPATH="$2"
    shift # past argument
    ;;
    -v|--verbose)
    VERBOSE="$2"
    shift # past argument
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done
echo OUTPUT PATH  = "${OUTPUTPATH}"
echo OUTPUT DIRECTORY TREE  = "${OUTPUTDIRECTORYTREE}"
echo SEARCH PATH     = "${SEARCHPATH}"

scriptexecutiondirectorytemp="$PWD"
find "${SEARCHPATH}" -type f -name '*.shortcut' | while read fullfilename; do
    echo "Processing $fullfilename"
    fulldirname="${fullfilename%/*}"
    filename=$(basename "$fullfilename")
    fname="${filename%.*}"
    
    cd "$fulldirname"
    symlinktargetrelativepath=`cat "$fname.shortcut"`
    echo "symlinktargetrelativepath $symlinktargetrelativepath / fulldirname $fulldirname"
    ln -s "$symlinktargetrelativepath" "$fname.html"
    rm "$fname.shortcut"
    cd "$scriptexecutiondirectorytemp"
done
