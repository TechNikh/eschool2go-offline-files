#!/bin/bash

OUTSTATUS="$(git diff --name-status --diff-filter=U)"
while read -r line; do
  if [[ $line == *".md"* ]]
  then
	FILEPATH=`echo $line| cut -c3-250`
	echo "Removing ...$FILEPATH..."
	# Remove now and in the next attempt in running the script will work
	rm "$FILEPATH"
	git rm "$FILEPATH"
  fi
done <<< "$OUTSTATUS"
#git commit -m "Auto Merging after resolving conflicts."
    